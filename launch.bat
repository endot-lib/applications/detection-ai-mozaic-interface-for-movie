@echo off
setlocal

:: 引数が指定されていない場合のデフォルト値を設定
set "MAX_SIZE=%~1"
set "MAX_SIZE=%MAX_SIZE:640%"
set "TIMELINE_THUMBNAIL_NUM=%~2"
set "TIMELINE_THUMBNAIL_NUM=%TIMELINE_THUMBNAIL_NUM:100%"

:: 仮想環境のパスを設定
set "venvDir=venv"

:: 仮想環境が存在しない場合は作成
if not exist "%venvDir%" (
    python -m venv "%venvDir%"
)

:: 仮想環境をアクティベート
call "%venvDir%\Scripts\activate.bat"

:: 必要なパッケージをインストール
pip install -r requirements.txt

:: アプリケーションを起動
python run.py --MAX_SIZE=%MAX_SIZE% --TIMELINE_THUMBNAIL_NUM=%TIMELINE_THUMBNAIL_NUM%

:: 仮想環境をデアクティベート
call "%venvDir%\Scripts\deactivate.bat"

endlocal
