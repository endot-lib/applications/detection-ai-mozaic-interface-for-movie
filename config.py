# config.py
import os

# run.py が存在するディレクトリ（つまりリポジトリのルート）への絶対パスを取得
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# BASE_DIR からの相対パスを使用して outputs ディレクトリへのパスを設定
OUTPUTS_DIR = os.path.join(BASE_DIR, 'outputs')

# モデルディレクトリへのパスを設定
MODELS_DIR = os.path.join(BASE_DIR, 'models')