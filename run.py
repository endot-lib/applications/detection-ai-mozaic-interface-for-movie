from backend import create_app
from backend.socketio_instance import socketio
from flask_cors import CORS
import torch

app = create_app()

# SocketIOのCORS設定
CORS(app, resources={r"*": {"origins": "*"}})
socketio.init_app(app, cors_allowed_origins="*")

# FlaskのCORS設定

@socketio.on('connect')
def handle_connect():
    print('Client connected')

@socketio.on('disconnect')
def handle_disconnect():
    print('Client disconnected')

if __name__ == '__main__':

    import argparse

    # コマンドライン引数を処理する
    parser = argparse.ArgumentParser(description='Launch options')
    parser.add_argument('--MAX_SIZE', dest='MAX_SIZE', type=int, help='プレビュー画像の最大縦横サイズ')
    parser.add_argument('--TIMELINE_THUMBNAIL_NUM', type=int, help='タイムラインに表示するサムネイルの数')
    args = parser.parse_args()

    # 引数が指定されていない場合に警告を表示
    if args.MAX_SIZE is None or args.TIMELINE_THUMBNAIL_NUM is None:
        app.logger.warning("警告: MAX_SIZEまたはTIMELINE_THUMBNAIL_NUMが指定されていません。")
    else:
        # 引数の値をFlaskアプリケーションの設定に追加
        app.config['MAX_SIZE'] = args.MAX_SIZE
        app.config['TIMELINE_THUMBNAIL_NUM'] = args.TIMELINE_THUMBNAIL_NUM

    # その他の設定
    app.logger.setLevel('DEBUG')

    # GPU確認
    app.logger.info("GPU is %s", "available" if torch.cuda.is_available() else "not available")
    socketio.run(app, debug=True)
