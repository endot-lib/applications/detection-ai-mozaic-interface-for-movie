from pydantic import BaseModel, root_validator, ValidationError
from typing import List, Dict
import numpy as np

# 座標データのモデル定義
class Coordinate(BaseModel):
    x: float
    y: float
class CoordinatesList(BaseModel):
    coordinates: List[Coordinate]

# リクエストデータのモデル定義
class ImageConvertRequest(BaseModel):
    frame_index: int
    pen_size: int
    coordinates: List[Coordinate]
    mosaic_size: int
    frame_type: str # processed | visualized

class MaskData(BaseModel):
    coordinates: List[Coordinate]
    input_type: str # manual, detect, segment
    mosaic_size: int

class FrameIndexWithMaskData(BaseModel):
    frames: Dict[int, List[MaskData]]

    def add_mask_data(self, frame_index: int, mask_data: MaskData):
        if frame_index not in self.frames:
            self.frames[frame_index] = []
        self.frames[frame_index].append(mask_data)

FrameDataDType = np.dtype([
    ('image_data', 'O'), # オブジェクト型(U(文字列型)だと入りきらない)
    ('is_processed', 'bool')  # ブール型
])


class RunAutoDetectionRequest(BaseModel):
    frameRange: list[int]  # frameRangeを保持
    from_frame: int = 0    # デフォルト値を設定、カスタムバリデータで上書きされる
    to_frame: int = 0      # デフォルト値を設定、カスタムバリデータで上書きされる
    mosaicExpandSize: int
    mosaicThreshold: float # モザイクのしきい値
    mosaicSize: int # モザイクのサイズ
    isAutoMosaicSize: bool
    selectedDetectModel: str
    selectedSegmentModel: str

    # frameRangeからfrom_frameとto_frameを設定するroot_validator
    @root_validator(pre=True)
    def extract_from_to_frame(cls, values):
        frameRange = values.get('frameRange', [])
        if len(frameRange) == 2:
            values['from_frame'], values['to_frame'] = frameRange
        return values
