from backend.models import FrameIndexWithMaskData, FrameDataDType
import numpy as np

class AppParamsSingleton:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(AppParamsSingleton, cls).__new__(cls, *args, **kwargs)

            # アップロードされた動画ファイルの一時ファイルパスを保持する
            cls.temp_video_path: str = ''

            # 縮小してBase64エンコードされた画像データ（文字列）の配列（フロントの描画用）これは描画用オリジナル配列
            cls.frames_data_base64 = np.empty((0,), dtype=FrameDataDType)

            # `frames_data_base64`を加工した画像を入れる配列
            cls.processed_frames_data_base64 = np.empty((0,), dtype=FrameDataDType)

            # `frames_data_base64`の座標部分を可視化した画像を入れる配列
            cls.processed_frames_data_base64_with_mask = np.empty((0,), dtype=FrameDataDType)

            # 画像の縮小率
            cls.resize_ratio: float = 0
            # 書き出す際のマスク情報を保持する配列
            cls.mask_with_frame_index: FrameIndexWithMaskData = FrameIndexWithMaskData(frames={})
        return cls._instance

    # データを初期化する
    def reset_data(self):
        self.temp_video_path = ''
        self.frames_data_base64 = np.empty((0,), dtype=FrameDataDType)
        self.processed_frames_data_base64 = np.empty((0,), dtype=FrameDataDType)
        self.processed_frames_data_base64_with_mask = np.empty((0,), dtype=FrameDataDType)
        self.resize_ratio = 0
        self.mask_with_frame_index = FrameIndexWithMaskData(frames={})
