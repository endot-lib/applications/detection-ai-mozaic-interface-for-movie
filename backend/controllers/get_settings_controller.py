from flask import jsonify, current_app

def get_settings():
    # ユーザ設定値を取得
    max_size = current_app.config['MAX_SIZE']
    timeline_thumbnail_num = current_app.config['TIMELINE_THUMBNAIL_NUM']
    # 設定値をJSON形式で返す
    return jsonify({
        'maxSize': max_size,
        'timelineThumbnailNum': timeline_thumbnail_num
        })
