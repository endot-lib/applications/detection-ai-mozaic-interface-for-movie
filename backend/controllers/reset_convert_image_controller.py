from flask import request, jsonify
from backend.singleton.AppParamsSingleton import AppParamsSingleton
from backend.utils.cache_clear import cache_clear
from backend.models import FrameDataDType
import numpy as np

def get_app_params_singleton():
    return AppParamsSingleton()

def is_frame_index_valid(frame_index, frames_list):
    return 0 <= frame_index < len(frames_list)

def reset_convert_image():
    """
    指定されたフレームの元画像を返す（モザイクをリセットしたい）
    """
    data = request.get_json()
    frame_index = data.get('frame_index')

    APS = get_app_params_singleton()

    if not len(APS.processed_frames_data_base64):
        return jsonify({'error': 'No video uploaded', 'code': 400}), 400

    if not is_frame_index_valid(frame_index, APS.processed_frames_data_base64):
        return jsonify({'error': 'Frame index out of range', 'code': 400}), 400

    # 加工画像を元画像にリセットする
    APS.processed_frames_data_base64[frame_index] = np.array(
        [(APS.frames_data_base64[frame_index]['image_data'], False)], dtype=FrameDataDType
        )[0]

    # 加工＆マスク画像を元画像にリセットする
    APS.processed_frames_data_base64_with_mask[frame_index] = np.array(
        [(APS.frames_data_base64[frame_index]['image_data'], False)], dtype=FrameDataDType
        )[0]

    # キャッシュをクリアする
    cache_clear('get_frame')

    response = jsonify({
        'frame_image': APS.processed_frames_data_base64[frame_index]['image_data'],
        'is_processed': int(APS.processed_frames_data_base64[frame_index]['is_processed']),
    })
    response.headers['Cache-Control'] = 'public, max-age=1800'  # 30分間キャッシュする
    return response
