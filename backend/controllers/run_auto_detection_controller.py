import numpy as np
import cv2
import torch
from flask import request, jsonify, current_app
from config import MODELS_DIR
from backend.models import RunAutoDetectionRequest, MaskData, Coordinate, FrameDataDType
from pydantic import ValidationError
from backend.singleton.AppParamsSingleton import AppParamsSingleton
from ultralytics import YOLO
from backend.utils.apply_mosaic import apply_mosaic
from backend.utils.decode_base64_to_image import decode_base64_to_image
from backend.utils.encode_image_to_base64 import encode_image_to_base64
from backend.utils.create_polygon_mask import create_polygon_mask
from backend.utils.visualize_mosaic_area import visualize_mosaic_area
from typing import List
from backend.socketio_instance import socketio

# モデルの読み込み処理を関数化する
def _load_model(model_path: str) -> YOLO:
    return YOLO(model_path)

def _save_and_apply_mosaic_to_image(
        frame_index: int,
        mask: np.ndarray,
        app_params_singleton: AppParamsSingleton,
        mosaic_size: int,
        ):
    """シングルトンに加工した画像を保存する処理"""
    # モザイク処理をして保存
    base_img_cv2 = decode_base64_to_image(app_params_singleton.processed_frames_data_base64[frame_index]['image_data'])
    processed_img_cv2 = apply_mosaic(base_img_cv2, mask, mosaic_size)
    processed_image_base64 = encode_image_to_base64(processed_img_cv2)
    app_params_singleton.processed_frames_data_base64[frame_index] = np.array(
        [(processed_image_base64, True)], dtype=FrameDataDType
        )[0]
    # モザイク＆可視化した画像を保存
    visualize_base_img_cv2 = decode_base64_to_image(app_params_singleton.processed_frames_data_base64_with_mask[frame_index]['image_data'])
    visualization_img_cv2 = visualize_mosaic_area(visualize_base_img_cv2, mask, mosaic_size)
    visualization_image_base64 = encode_image_to_base64(visualization_img_cv2)
    app_params_singleton.processed_frames_data_base64_with_mask[frame_index] = np.array(
        [(visualization_image_base64, True)], dtype=FrameDataDType
        )[0]

def _apply_mosaic_and_save_mask_data(
        app_params_singleton: AppParamsSingleton,
        origin_coords: List[Coordinate],
        resized_coords: List[Coordinate],
        frame_index: int,
        mosaic_size: int,
        ):
    """マスクを作成し、頂点座標を生成してadd_mask_data関数で保存する処理"""
    # モザイク処理 まずべーすとなる画像を取得
    base_img_cv2 = decode_base64_to_image(app_params_singleton.frames_data_base64[frame_index]['image_data'])
    # マスクを作成
    mask = create_polygon_mask(base_img_cv2.shape, resized_coords)
    _save_and_apply_mosaic_to_image(frame_index, mask, app_params_singleton, mosaic_size)
    # マスクを作成し、頂点座標を生成してadd_mask_data関数で保存する
    mask_data = MaskData(
        coordinates=origin_coords,
        input_type='segment',
        mosaic_size=mosaic_size
    )
    # マスクデータを保存
    app_params_singleton.mask_with_frame_index.add_mask_data(frame_index, mask_data)

def _expand_coordinates(coords: List[Coordinate], expand_percentage: float) -> List[Coordinate]:
    """形状、比率を保ったまま範囲を拡張する処理"""
    # ポリゴンの重心を計算
    centroid_x = sum(coord.x for coord in coords) / len(coords)
    centroid_y = sum(coord.y for coord in coords) / len(coords)

    expanded_coords = []
    for coord in coords:
        # 重心からの相対距離に基づいて各頂点を移動
        relative_x = coord.x - centroid_x
        relative_y = coord.y - centroid_y

        # 拡張比率に基づく距離の計算
        expanded_x = centroid_x + relative_x * (1 + expand_percentage / 100)
        expanded_y = centroid_y + relative_y * (1 + expand_percentage / 100)

        # 拡張された座標の追加
        expanded_coords.append(Coordinate(x=expanded_x, y=expanded_y))

    return expanded_coords


def run_auto_detection():
    try:
        request_data = RunAutoDetectionRequest(**request.get_json())
        current_app.logger.info(request_data.model_dump())
        app_params_singleton = AppParamsSingleton()

        # 動画の読み込みとフレームレートの取得
        cap = cv2.VideoCapture(app_params_singleton.temp_video_path)

        # modelのオプション設定
        model_op_conf = request_data.mosaicThreshold
        model_op_mosaic_expand_size = request_data.mosaicExpandSize
        model_op_mosaic_size = request_data.mosaicSize
        # TODO [ ] is_auto_mosaic_sizeこれはいらないかも一旦保留
        # model_op_is_auto_mosaic_size = request_data.isAutoMosaicSize

        # GPUデバイス確認
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        # detectモデルの読み込み
        detect_model = _load_model(MODELS_DIR + '/detect_models/' + request_data.selectedDetectModel).to(device)
        detect_model.conf = model_op_conf # 検出するしきい値の設定
        # segmentモデルの読み込み
        segment_model = _load_model(MODELS_DIR + '/segment_models/' + request_data.selectedSegmentModel).to(device)
        segment_model.conf = model_op_conf # 検出するしきい値の設定

        # トラッキングの実行
        frame_index = 0
        ratio = app_params_singleton.resize_ratio

        # フロントに進捗情報を送信
        socketio.emit('runAIProgress', {
            'message': 'Detecting objects...',
            'total_num': request_data.to_frame - request_data.from_frame,
            'current_num': 0
        })

        while True:
            ret, frame = cap.read()
            if not ret or frame_index > request_data.to_frame -1:
                break # フレームが読み込めなかったらループを抜ける

            # フレームが処理対象範囲内かどうかを判定（範囲を超えたらループを抜ける）
            if frame_index >= request_data.from_frame -1:
                # segment版トラッキングの物体検出を行う
                segment_results = segment_model.track(frame, conf=model_op_conf, persist=True, stream=True, verbose=False)
                for result in segment_results:
                    if result.boxes is not None and len(result.boxes.xyxy) > 0:
                        for polygon_coords in result.masks.xy:
                            # mosaicExpandSizeで指定された割合で座標を拡張
                            expanded_segment_coords = _expand_coordinates(
                                [Coordinate(x=x, y=y) for x, y in polygon_coords],
                                model_op_mosaic_expand_size,
                                )
                            # リサイズ比で調整
                            resized_polygon_coords = [Coordinate(x=c.x * ratio, y=c.y * ratio) for c in expanded_segment_coords]
                            # マスクを作成し、頂点座標を生成してadd_mask_data関数で保存する
                            _apply_mosaic_and_save_mask_data(
                                app_params_singleton,
                                expanded_segment_coords,
                                resized_polygon_coords,
                                frame_index,
                                model_op_mosaic_size,
                                )

                # detect版トラッキングの物体検出を行う
                detect_results = detect_model.track(frame, conf=model_op_conf, persist=True, stream=True, verbose=False)
                for result in detect_results:
                    if result.boxes is not None and len(result.boxes.xyxy) > 0:
                        for box in result.boxes:
                            if torch.any(box.cls == 1):
                                continue
                            x1, y1, x2, y2 = box.xyxy[0].tolist()
                            # mosaicExpandSizeで指定された割合で座標を拡張
                            expanded_detect_coords = _expand_coordinates(
                                [
                                    Coordinate(x=x1, y=y1),
                                    Coordinate(x=x2, y=y1),
                                    Coordinate(x=x2, y=y2),
                                    Coordinate(x=x1, y=y2),
                                ],
                                model_op_mosaic_expand_size,
                                )
                            # 拡張された座標をリサイズ比で調整
                            resized_box_coords = [
                                Coordinate(x=coord.x * ratio, y=coord.y * ratio) for coord in expanded_detect_coords
                            ]
                            # マスクを作成し、頂点座標を生成してadd_mask_data関数で保存する
                            _apply_mosaic_and_save_mask_data(
                                app_params_singleton,
                                expanded_detect_coords,
                                resized_box_coords,
                                frame_index,
                                model_op_mosaic_size,
                                )

                # フロントに進捗情報を送信
                socketio.emit('runAIProgress', {
                    'message': 'Detecting objects...',
                    'total_num': request_data.to_frame - request_data.from_frame,
                    'current_num': frame_index - request_data.from_frame +1
                })

            frame_index += 1

        cap.release()
        return jsonify({"message": "成功"}), 200

    except ValidationError as e:
        # バリデーションエラーの場合は、エラーメッセージをレスポンスとして返す
        return jsonify({"errors": e.errors()}), 400
    except Exception as e:
        # その他のエラーの場合
        print(e)
        return jsonify({"error": str(e)}), 500
