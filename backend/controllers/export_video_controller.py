import os
import datetime
from moviepy.editor import VideoFileClip
from backend.singleton.AppParamsSingleton import AppParamsSingleton
from config import OUTPUTS_DIR
from backend.socketio_instance import socketio
from backend.utils.adjust_maskdata import adjust_maskdata
from backend.utils.apply_mosaic_with_mask_moviepy import apply_mosaic_with_mask_moviepy

def export_video() -> str: # ファイル名を返す（ダウンロード時に必要）

    def send_progress(frame_index, total_frames):
        # 進捗情報をフロントに送信する処理
        socketio.emit('exportProgress', {
            'frameCount': total_frames,
            'currentFrameIndex': frame_index,
        })

    def process_frame(frame):
        # 各フレームにモザイクを適用する処理
        frame_index = clip.reader.pos - 1
        if frame_index in adjusted_maskdata.frames:
            for mask_data in adjusted_maskdata.frames[frame_index]:
                frame = apply_mosaic_with_mask_moviepy(frame, mask_data)
        # 進捗情報をフロントに送信する処理
        send_progress(frame_index, total_frames)
        return frame

    # outputディレクトリがなければ作る
    if not os.path.exists(OUTPUTS_DIR):
        os.makedirs(OUTPUTS_DIR)

    # APSのインスタンスを取得
    app_params_singleton = AppParamsSingleton()
    # 動画の読み込み
    clip = VideoFileClip(app_params_singleton.temp_video_path)
    # 総フレーム数の計算
    total_frames = int(clip.fps * clip.duration)
    # 拡縮を再調整した新しい座標リストを作成
    adjusted_maskdata = adjust_maskdata(
        app_params_singleton.mask_with_frame_index,
        app_params_singleton.resize_ratio
    )
    # 動画の書き出し処理
    new_clip = clip.fl_image(process_frame)
    filename = 'exportfile_' + datetime.datetime.now().strftime('%Y%m%d%H%M%S') + '.mp4'
    output_file = OUTPUTS_DIR + '/' + filename
    new_clip.write_videofile(output_file, codec='libx264', audio_codec='aac')

    return filename
