from flask import send_from_directory
from config import OUTPUTS_DIR

def get_tmp_file(filename: str):
    """outputsディレクトリ内に生成されたファイルを取得してそのまま返す"""
    return send_from_directory(OUTPUTS_DIR, filename)
