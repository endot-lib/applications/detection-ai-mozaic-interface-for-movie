from flask import jsonify
# from functools import lru_cache
from backend.singleton.AppParamsSingleton import AppParamsSingleton

# @lru_cache(maxsize=None)  # キャッシュの適用 2024年2月21日13時38分色々面倒なので一旦コメントアウト
def get_frame(frame_index: int, image_type: str):

    app_params_singleton = AppParamsSingleton()
    # NumPy 配列が空かどうかをチェック
    if app_params_singleton.processed_frames_data_base64.size == 0:
        return 'No video uploaded', 400
    # フレーム番号が範囲内かどうかをチェック
    if frame_index < 0 or frame_index >= app_params_singleton.processed_frames_data_base64.size:
        return 'Frame index out of range', 400

    frame_image = None
    if image_type == 'processed':
        frame_image = app_params_singleton.processed_frames_data_base64[frame_index]['image_data']
        is_processed = app_params_singleton.processed_frames_data_base64[frame_index]['is_processed']
    elif image_type == 'visualized':
        frame_image = app_params_singleton.processed_frames_data_base64_with_mask[frame_index]['image_data']
        is_processed = app_params_singleton.processed_frames_data_base64_with_mask[frame_index]['is_processed']
    else:
        return 'Invalid image type', 400

    response = jsonify({
        'frame_index': frame_index,
        'frame_type': image_type,
        'frame_image': frame_image,
        'is_processed': int(is_processed), # int型に変換して返す
    })
    response.headers['Cache-Control'] = 'public, max-age=1800'  # 30分間キャッシュする
    return response
