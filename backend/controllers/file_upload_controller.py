import cv2
import base64
import gc
import tempfile
import numpy as np
import torch
from flask import request, jsonify, current_app
from backend.models import FrameDataDType
from backend.singleton.AppParamsSingleton import AppParamsSingleton
from backend.socketio_instance import socketio
from tqdm import tqdm

def downsize_video_gpu(input_path, output_path, target_width, device, socketio):
    cap = cv2.VideoCapture(input_path)

    # 元の動画の幅と高さを取得
    original_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    original_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # アスペクト比を維持してリサイズ
    aspect_ratio = original_width / original_height
    target_height = int(target_width / aspect_ratio)

    # ダウンサイズ後の動画ファイルを作成
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_path, fourcc, cap.get(cv2.CAP_PROP_FPS), (target_width, target_height))

    # 動画の総フレーム数を取得
    total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    # tqdmを使用して進捗バーを表示
    with tqdm(total=total_frames, unit='frame') as pbar:
        while True:
            ret, frame = cap.read()
            if not ret:
                break

            # フレームをPytorchのテンソルに変換
            frame_tensor = torch.from_numpy(frame).to(device).permute(2, 0, 1).unsqueeze(0).float()

            # GPUでリサイズ
            resized_frame_tensor = torch.nn.functional.interpolate(
                frame_tensor,
                size=(target_height, target_width),
                mode='bilinear',
                align_corners=False
            )

            # テンソルをNumPy配列に変換
            resized_frame = resized_frame_tensor.squeeze(0).permute(1, 2, 0).cpu().numpy().astype('uint8')

            out.write(resized_frame)

            # GPU メモリを開放
            del frame_tensor, resized_frame_tensor
            torch.cuda.empty_cache()

            # 進捗バーを更新
            pbar.update(1)
            socketio.emit('uploadProgress', {'message': f'Down sizing video... {pbar.n}/{total_frames}'})

    cap.release()
    out.release()

    # ガベージコレクションを実行
    gc.collect()

def file_upload():
    current_app.logger.info('file uploading...')
    gc.collect()  # ガベージコレクションを強制的に実行

    app_params_singleton = AppParamsSingleton()
    app_params_singleton.reset_data()

    # GPUデバイスを取得
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # フロントにアップロード開始を通知
    socketio.emit('uploadProgress', {'message': 'STEP.1/5 Prepare and Down sizing video...'})
    file = request.files['video']
    with tempfile.NamedTemporaryFile(delete=False, suffix='.mp4') as temp_file:
        file.save(temp_file.name)
        temp_video_path = temp_file.name

    # 動画をダウンサイズ
    downsized_video_path = tempfile.NamedTemporaryFile(delete=False, suffix='.mp4').name
    target_width = 640  # ダウンサイズ後の幅を指定
    downsize_video_gpu(temp_video_path, downsized_video_path, target_width, device, socketio)

    # ダウンサイズされた動画を読み込んで処理を開始
    cap = cv2.VideoCapture(downsized_video_path)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    # 一時的な配列を作成
    tmp_frames_data_base64_list = []

    # バッチサイズを設定
    batch_size = 100

    # フレームの読み込み
    socketio.emit('uploadProgress', {'message': 'STEP.2/5 Reading frames...'})

    for batch_start in tqdm(range(0, frame_count, batch_size), desc="Processing batches"):
        # 進捗を表示
        socketio.emit('uploadProgress', {'message': f'STEP.2/5 Reading frames... {batch_start}/{frame_count}'})
        frames = []
        for frame_idx in range(batch_start, min(batch_start + batch_size, frame_count)):
            success, frame = cap.read()
            if not success:
                break
            frames.append(frame)

        # フレームをGPUに転送
        frames_tensor = torch.from_numpy(np.stack(frames)).to(device).float()  # データ型をfloat32に変換
        # 画像をリサイズ
        resized_frames_tensor = torch.nn.functional.interpolate(
            frames_tensor.permute(0, 3, 1, 2),
            scale_factor=current_app.config['MAX_SIZE'] / max(frames_tensor.shape[2], frames_tensor.shape[1]),
            mode='bilinear',
            align_corners=False
        ).permute(0, 2, 3, 1)

        # リサイズ率を設定
        app_params_singleton.resize_ratio = resized_frames_tensor.shape[2] / frames_tensor.shape[2]

        # リサイズ後の画像をGPUでエンコード
        encoded_frames = []
        for frame in resized_frames_tensor:
            frame_np = frame.cpu().numpy()
            _, encoded_frame = cv2.imencode('.jpg', frame_np, [int(cv2.IMWRITE_JPEG_QUALITY), 50])
            encoded_frames.append(encoded_frame)

        base64_images = [base64.b64encode(buffer).decode('utf-8') for buffer in encoded_frames]

        # NumPy配列のデータ型を定義
        frame_data = np.array([(image, False) for image in base64_images], dtype=FrameDataDType)
        tmp_frames_data_base64_list.extend(frame_data)

        # リサイズ後のテンソルを保持
        app_params_singleton.resized_frames_tensor = resized_frames_tensor

        # メモリを開放
        del frames, frames_tensor, resized_frames_tensor, encoded_frames, base64_images, frame_data
        gc.collect()

    cap.release()

    # NumPy配列に変換
    app_params_singleton.frames_data_base64 = np.array(tmp_frames_data_base64_list, dtype=FrameDataDType)

    # コピーを作成（加工修正を戻すとき用）
    app_params_singleton.processed_frames_data_base64 = np.array(tmp_frames_data_base64_list, dtype=FrameDataDType)

    # 座標を可視化した画像を入れる配列用にもコピー
    app_params_singleton.processed_frames_data_base64_with_mask = np.array(tmp_frames_data_base64_list, dtype=FrameDataDType)

    # 一時的に作成した配列を削除
    del tmp_frames_data_base64_list

    # フロントにアップロード完了を通知
    socketio.emit('uploadProgress', {'message': 'Upload completed'})

    # フレーム数を更新
    frame_count = len(app_params_singleton.frames_data_base64)

    return jsonify({
        'frame_count': frame_count,
        'frame_image': app_params_singleton.frames_data_base64[0]['image_data'] if frame_count > 0 else None,
        'resize_ratio': app_params_singleton.resize_ratio,
        'resized_width': app_params_singleton.resized_frames_tensor.shape[3] if frame_count > 0 else 0,
        'resized_height': app_params_singleton.resized_frames_tensor.shape[2] if frame_count > 0 else 0,
    })