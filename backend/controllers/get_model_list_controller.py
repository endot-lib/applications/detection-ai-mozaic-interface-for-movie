import os
from flask import jsonify
from config import MODELS_DIR

def get_model_list():
    # サブディレクトリを指定
    directories = ['detect_models', 'segment_models']
    # 返却用の辞書を初期化
    models_dict = {}

    for directory in directories:
        dir_path = os.path.join(MODELS_DIR, directory)
        if os.path.isdir(dir_path):
            # .ptファイルのみを取得
            models_dict[directory] = [file for file in os.listdir(dir_path) if file.endswith('.pt')]
    return jsonify({'models': models_dict})
