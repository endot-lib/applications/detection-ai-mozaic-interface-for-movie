from flask import request, jsonify
import cv2
import numpy as np
from flask import current_app
from pydantic import ValidationError
from backend.singleton.AppParamsSingleton import AppParamsSingleton
from backend.models import ImageConvertRequest, MaskData, FrameDataDType, Coordinate
from backend.utils.cache_clear import cache_clear
from backend.utils.apply_mosaic import apply_mosaic
from backend.utils.decode_base64_to_image import decode_base64_to_image
from backend.utils.encode_image_to_base64 import encode_image_to_base64
from backend.utils.create_mask import create_mask
from backend.utils.visualize_mosaic_area import visualize_mosaic_area

DEFAULT_MOSAIC_SIZE: int = 10

def convert_image():
    def get_image_data(request_data: ImageConvertRequest) -> str:
        if request_data.frame_type == 'processed':
            return APS.processed_frames_data_base64[request_data.frame_index]['image_data']
        elif request_data.frame_type == 'visualized':
            return APS.processed_frames_data_base64_with_mask[request_data.frame_index]['image_data']
        else:
            return None  # または適切なデフォルト値
    try:
        APS = AppParamsSingleton()
        data = request.get_json()
        request_data = ImageConvertRequest(**data)

        # モザイク処理用の元画像と可視化処理用の元画像を取得
        mosaic_base_img_cv2 = decode_base64_to_image(APS.processed_frames_data_base64[request_data.frame_index]['image_data'])
        # 一度だけマスクを作成
        mask = create_mask(mosaic_base_img_cv2.shape, request_data.coordinates, request_data.pen_size)
        # モザイク処理
        mosaic_size = request_data.mosaic_size if request_data.mosaic_size else DEFAULT_MOSAIC_SIZE
        processed_img_cv2 = apply_mosaic(mosaic_base_img_cv2, mask, mosaic_size)
        # Base64エンコード
        processed_image_base64 = encode_image_to_base64(processed_img_cv2)

        # モザイクされた領域に青色の半透明のレイヤーを重ねる
        visualize_base_img_cv2 = decode_base64_to_image(APS.processed_frames_data_base64_with_mask[request_data.frame_index]['image_data'])
        # 可視化画像の生成処理
        visualization_img_cv2 = visualize_mosaic_area(visualize_base_img_cv2, mask, mosaic_size)
        # Base64エンコード
        visualization_image_base64 = encode_image_to_base64(visualization_img_cv2)
        # マスクを作成し、頂点座標を生成してadd_mask_data関数で保存する
        contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        mask_data = MaskData(
            # 手動での範囲選択なので都度マスクが生成されるので、リストの最初の要素を取得でOK
            coordinates = [Coordinate(x=coord[0][0], y=coord[0][1]) for coord in contours[0]],
            input_type = 'manual',
            mosaic_size = mosaic_size
        )

        # マスクデータを保存
        APS.mask_with_frame_index.add_mask_data(request_data.frame_index, mask_data)
        # モザイクした画像を保存
        APS.processed_frames_data_base64[request_data.frame_index] = np.array(
            [(processed_image_base64, True)], dtype=FrameDataDType
            )[0]

        # モザイク＆可視化した画像を保存
        APS.processed_frames_data_base64_with_mask[request_data.frame_index] = np.array(
            [(visualization_image_base64, True)], dtype=FrameDataDType
            )[0]

        cache_clear('get_frame') # キャッシュをクリアする

        return jsonify({
            "message": "Image processed successfully",
            "convert_image": get_image_data(request_data),
            "is_processed": int(APS.processed_frames_data_base64[request_data.frame_index]['is_processed']),
        })

    except ValidationError as e:
        current_app.logger.error(f"ValidationError: {e}")
        return jsonify({"error": str(e)}), 400
    except Exception as e:
        current_app.logger.error(f"Unexpected error: {e}")
        return jsonify({"error": "An unexpected error occurred"}), 500