from flask import request, jsonify
from backend.singleton.AppParamsSingleton import AppParamsSingleton

def get_frames():
    """
    フロントからほしいフレームのインデックスのリストを受け取るので、それを返す
    """
    app_params_singleton = AppParamsSingleton()
    data = request.get_json()
    frame_images: list = []
    for frame_index in data['frame_indexs']:
        frame_images.append(app_params_singleton.processed_frames_data_base64[frame_index]['image_data'])

    response = jsonify({'frame_images': frame_images})
    response.headers['Cache-Control'] = 'public, max-age=1800'  # 30分間キャッシュする
    return response
