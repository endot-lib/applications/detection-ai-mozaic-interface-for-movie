# リセット処理を行うコントローラー
from flask import jsonify
from backend.singleton.AppParamsSingleton import AppParamsSingleton
from backend.utils.cache_clear import cache_clear

def reset():
    # シングルトンのインスタンスを取得
    app_params_singleton = AppParamsSingleton()
    # データを初期化
    app_params_singleton.reset_data()
    # キャッシュをクリア
    cache_clear('get_frame')

    return jsonify({'message': 'Reset successfully'})
