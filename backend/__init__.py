from flask import Flask
from .routes import main_routes

def create_app():
    app = Flask(__name__, static_folder='../app/build', static_url_path='')
    app.register_blueprint(main_routes)

    @app.after_request
    def add_cache_control_header(response):
        response.headers['Cache-Control'] = 'public, max-age=3600' # 1時間キャッシュする
        return response

    return app
