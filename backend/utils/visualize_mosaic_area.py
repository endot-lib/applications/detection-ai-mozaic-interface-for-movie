import cv2
import numpy as np
from backend.utils.apply_mosaic import apply_mosaic

# モザイクとその視覚化を行う関数
def visualize_mosaic_area(img_cv2, mask, mosaic_size) -> np.ndarray:
    # モザイク処理
    mosaiced_img = apply_mosaic(img_cv2.copy(), mask, mosaic_size)

    # 半透明の青で塗りつぶす
    alpha = 0.3
    blue_overlay = np.full_like(mosaiced_img, (0, 0, 255))
    mask_blue = mask > 0
    mask_uint8 = mask_blue.astype(np.uint8) * 255  # ブール配列をuint8型に変換
    mosaiced_img[mask_blue] = (1.0 - alpha) * mosaiced_img[mask_blue] + alpha * blue_overlay[mask_blue]

    # 輪郭線生成
    contour_layer = np.zeros_like(mosaiced_img)
    contours, _ = cv2.findContours(mask_uint8, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)  # 修正: mask_uint8を使用
    cv2.drawContours(contour_layer, contours, -1, (255, 255, 255), 1)

    # 輪郭線を合成
    alpha_line = 0.3
    mask_line = contour_layer.any(axis=2)
    mosaiced_img[mask_line] = (1.0 - alpha_line) * mosaiced_img[mask_line] + alpha_line * contour_layer[mask_line]

    return mosaiced_img