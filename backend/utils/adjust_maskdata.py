from backend.models import FrameIndexWithMaskData, MaskData, Coordinate

def adjust_maskdata(original_maskdata: FrameIndexWithMaskData, resize_ratio: float) -> FrameIndexWithMaskData:
    """
    リサイズされた画像のマスクデータをオリジナルの動画サイズに合わせて調整する関数。
    input_typeがmanualの場合のみ、リサイズ比率を適用して座標を調整する。

    :param original_maskdata: リサイズされた画像のマスクデータの辞書。キーはフレーム番号、値はMaskDataオブジェクトのリスト。
    :param resize_ratio: 元の画像に対するリサイズ比率。
    :return: 調整されたマスクデータの辞書。キーはフレーム番号、値は調整されたMaskDataオブジェクトのリスト。
    """
    adjusted_maskdata = FrameIndexWithMaskData(frames={})
    for frame_index, maskdata_list in original_maskdata.frames.items():
        adjusted_maskdata_list = []
        for maskdata in maskdata_list:
            adjusted_coordinates = []
            if maskdata.input_type == 'manual':
                # input_typeがmanualの場合、リサイズ比率に基づいて座標を調整
                for coord in maskdata.coordinates:
                    adjusted_x = coord.x / resize_ratio
                    adjusted_y = coord.y / resize_ratio
                    adjusted_coordinates.append(Coordinate(x=adjusted_x, y=adjusted_y))
            else:
                # detectやsegmentの場合は、座標の調整を行わない
                adjusted_coordinates = maskdata.coordinates

            # モザイクサイズの調整は全てのinput_typeで共通
            adjusted_mosaic_size = int(maskdata.mosaic_size / resize_ratio)

            adjusted_maskdata_obj = MaskData(
                coordinates=adjusted_coordinates,
                input_type=maskdata.input_type,
                mosaic_size=adjusted_mosaic_size
            )
            adjusted_maskdata_list.append(adjusted_maskdata_obj)
        adjusted_maskdata.frames[frame_index] = adjusted_maskdata_list

    return adjusted_maskdata