import cv2
import base64

def encode_image_to_base64(img_cv2) -> str:
    """ 画像をBase64にエンコードする関数 """
    _, encoded_image = cv2.imencode('.jpg', img_cv2, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
    return base64.b64encode(encoded_image).decode()