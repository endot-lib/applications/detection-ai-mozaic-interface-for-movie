import cv2
import numpy as np

def create_mask(img_shape, coordinates, pen_size) -> np.ndarray:
    """
    指定された座標とペンサイズを使用してマスクを作成する関数。

    Parameters:
    - img_shape: 対象の画像の形状（高さ、幅、チャンネル数）
    - coordinates: マスクを適用する座標のリスト。各座標は(x, y)の形式。
    - pen_size: マスクの描画に使用するペンのサイズ（円の半径）。

    Returns:
    - mask: 生成されたマスク画像（2値画像）。
    """
    # 画像と同じサイズの空のマスクを作成
    mask = np.zeros(img_shape[:2], dtype=np.uint8)
    for coord in coordinates:
        x_rounded, y_rounded = int(round(coord.x)), int(round(coord.y))
        cv2.circle(mask, (x_rounded, y_rounded), int(round(pen_size // 2)), 255, -1)
    return mask