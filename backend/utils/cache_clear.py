from backend.controllers.get_frame_controller import get_frame
from flask import current_app

def cache_clear(function_name: str = '') -> None:
    '''
    関数のキャッシュをクリアする

    Parameters function_name: str = get_frame
    '''
    switch = {
        # 'get_frame': get_frame.cache_clear() 一旦コメントアウトします 2024年2月21日 14時18分
    }
    if function_name in switch:
        current_app.logger.info(function_name)
        switch[function_name]

