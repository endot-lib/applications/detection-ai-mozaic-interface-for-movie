import cv2
import numpy as np
from ..models import CoordinatesList
from typing import List

def create_polygon_mask(img_shape: tuple, coordinates: List[CoordinatesList]) -> np.ndarray:
    """
    指定された座標リストからポリゴン領域全体をマスクとして作成する関数。

    Parameters:
    - img_shape: 対象の画像の形状（高さ、幅、チャンネル数）
    - coordinates: マスクを適用するポリゴン領域を定義する座標のリスト。
                   このリストはポリゴンの各頂点の(x, y)座標のタプルまたはCoordinateオブジェクトを含む。

    Returns:
    - mask: 生成されたマスク画像（2値画像）。ポリゴン領域が255、それ以外が0。
    """
    # 画像と同じサイズの空のマスクを作成
    mask = np.zeros(img_shape[:2], dtype=np.uint8)

    # 座標リストがCoordinateオブジェクトの場合、(x, y)ペアのリストに変換
    if hasattr(coordinates[0], 'x') and hasattr(coordinates[0], 'y'):
        pts = np.array([(coord.x, coord.y) for coord in coordinates], np.int32)
    else:
        pts = np.array(coordinates, np.int32)

    pts = pts.reshape((-1, 1, 2))
    # ポリゴン領域をマスク上に描画して塗りつぶす
    cv2.fillPoly(mask, [pts], 255)

    return mask

