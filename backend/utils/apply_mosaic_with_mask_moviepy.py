import numpy as np
import cv2
from backend.models import MaskData
from backend.utils.apply_mosaic import apply_mosaic

def apply_mosaic_with_mask_moviepy(frame, mask_data: MaskData) -> np.ndarray:
    """ マスクデータを使ってモザイク処理を行う関数 """
    frame_copy = frame.copy()
    mask = np.zeros(frame_copy.shape[:2], dtype=np.uint8)

    # 頂点座標を基にポリゴンマスクを生成
    if mask_data.coordinates:
        pts = np.array([[coord.x, coord.y] for coord in mask_data.coordinates], np.int32)
        pts = pts.reshape((-1, 1, 2))
        cv2.fillPoly(mask, [pts], 255)

    # モザイク処理
    return apply_mosaic(frame_copy, mask, mask_data.mosaic_size)
