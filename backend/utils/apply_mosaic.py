from typing import List
import cv2
# モザイク処理を行う外部関数
def apply_mosaic(img_cv2, mask, mosaic_size) -> List[int]:
    for i in range(0, mask.shape[0], mosaic_size):
        for j in range(0, mask.shape[1], mosaic_size):
            if mask[i:i+mosaic_size, j:j+mosaic_size].any():
                img_cv2[i:i+mosaic_size, j:j+mosaic_size] = cv2.blur(
                    img_cv2[i:i+mosaic_size, j:j+mosaic_size],
                    (mosaic_size, mosaic_size)
                    )
    return img_cv2