import cv2
import base64
import numpy as np

def decode_base64_to_image(img_base64: str) -> np.ndarray:
    """ Base64エンコードされた画像をデコードする関数 """
    img_data = base64.b64decode(img_base64)
    return cv2.imdecode(np.frombuffer(img_data, np.uint8), cv2.IMREAD_COLOR)