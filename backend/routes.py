from flask import Blueprint, send_from_directory
from .controllers.get_settings_controller import get_settings
from .controllers.file_upload_controller import file_upload
from .controllers.get_frame_controller import get_frame
from .controllers.reset_controller import reset
from .controllers.get_frames_controller import get_frames
from .controllers.convert_image_controller import convert_image
from .controllers.reset_convert_image_controller import reset_convert_image
from .controllers.export_video_controller import export_video
from .controllers.get_tmp_file_controller import get_tmp_file
from .controllers.get_model_list_controller import get_model_list
from .controllers.run_auto_detection_controller import run_auto_detection
from config import OUTPUTS_DIR

main_routes = Blueprint('main_route', __name__)

# @main_routes.route('/', methods=['GET'])
# def api_index():
#     return 'working!'

@main_routes.route('/', methods=['GET'])
def serve():
    return send_from_directory('../app/build', 'index.html')

# 動画系ファイルのアップロードを受け付ける
@main_routes.route('/file_upload', methods=['POST'])
def api_file_upload():
    return file_upload()

# 指定されたフレームの画像を返す
@main_routes.route('/frame/<int:frame_index>/image_type/<image_type>', methods=['POST'])
def api_get_frame(frame_index, image_type):
    return get_frame(frame_index, image_type)

# frame_indexのリストを受け取り、それに対応するフレームの画像を返す
@main_routes.route('/frames', methods=['POST'])
def api_get_frames():
    return get_frames()

# 読み込んだ動画や関連パラメータをリセットする
@main_routes.route('/reset', methods=['GET'])
def api_reset():
    return reset()

# 指定されたフレームに加工を施して返す
@main_routes.route('/convert_image', methods=['POST'])
def api_convert_image():
    return convert_image()

# 指定されたフレームの加工をリセットして返す
@main_routes.route('/reset_convert_image', methods=['POST'])
def api_reset_convert_image():
    return reset_convert_image()

# 動画をエクスポートするエンドポイント
@main_routes.route('/export_video', methods=['POST'])
def api_export_video() -> str:
    return export_video()

# 静的ファイルへのアクセスを許可する
# フロントからのリクエストはoutputsディレクトリで受け取るが内部的にはconfig.pyに定義されたOUTPUTS_DIRで受け取る
@main_routes.route(f'/outputs/<filename>', methods=['POST'])
def api_get_tmp_file(filename: str):
    return get_tmp_file(filename)

# modelsディレクトリの中にあるモデルのリストを返す
@main_routes.route('/models', methods=['POST'])
def api_get_model_list():
    return get_model_list()

# AIによるモザイク処理を実行するエンドポイント
@main_routes.route('/run_auto_detection', methods=['POST'])
def api_run_auto_detection():
    return run_auto_detection()

@main_routes.route('/settings', methods=['POST'])
def api_get_settings():
    return get_settings()
