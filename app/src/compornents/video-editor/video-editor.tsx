import React, { useState, useEffect } from 'react';
import styles from './style.module.sass';
import { FrameNavigation } from './frame-navigation';
import { PenSize } from './pen-size';
import { MosaicSize } from './mosaic-size';
import { KonvaCanvas } from './konva-canvas';
import { Coordinate } from '../../types/FlaskClientTypes';
import { ChangeFile } from './change-file';
import { FileReset } from './file-reset';
import { ExportButton } from './export-button';
import { AiDetectionTools } from './ai-detection-tools/ai-detection-tools';


export const VideoEditor: React.FC = () => {
    // 状態を管理したい変数
    const [frameImageUrl, setFrameImageUrl] = useState<string | null>(null); // 現在表示しているフレームの画像URL
    const [frameIndex, setFrameIndex] = useState<number>(0); // 現在表示しているフレームのインデックス
    const [frameCount, setFrameCount] = useState<number>(1); // 読み込んだ動画のフレーム数
    const [maskCoordinates, setMaskCoordinates] = useState<Coordinate[]>([]); // マスクの座標
    const [penSize, setPenSize] = useState<number>(30); // ペンのサイズ
    const [mosaicSize, setMosaicSize] = useState<number>(7); // モザイクのサイズ
    const [isAutoMosaicSize, setIsAutoMosaicSize] = useState<boolean>(false); // モザイクサイズを自動で算出するためのチェックボックス用の状態
    const [imageSize, setImageSize] = useState<{ width: number; height: number }>({ width: 0, height: 0 }); // 画像のサイズ
    const [isFileSelected, setisFileSelected] = React.useState<boolean>(false); // ファイルが選択されているかどうか
    // マスクモード関連
    const [isQuickMaskMode, setIsQuickMaskMode] = useState<boolean>(false); // マスクモードかどうかの状態
    const [isMaskmodeButtonActive, setIsMaskModeButtonActive] = useState<boolean>(false); // マスクモードをスイッチするボタン用の状態（クリックされたらON/OFFを切り替える）
    const [isMaskMode, setIsMaskMode] = useState<boolean>(false); // マスクモードかどうか
    // マウスカーソル
    const [cssCursorDeactive, setCssCursorDeactive] = useState(false);
    // 画像加工の痕跡があるかを表示するためのフラグ
    const [isProcessedImage, setIsProcessedImage] = useState<boolean>(false);

    useEffect(() => {
        // isQuickMaskMode, isMaskmodeButtonActiveに変化があったらisMaskModeを更新する
        // isMaskmodeButtonActiveがtrueなら全てtrue, isMaskmodeButtonActiveがfalseなら、isQuickMaskModeの値を反映する
        setIsMaskMode(isMaskmodeButtonActive || isQuickMaskMode);
    }, [isQuickMaskMode, isMaskmodeButtonActive])

    return (
        <>
            <div className={styles.videoEditor}>
                <div className={styles.mainColumn}>
                    <div className={styles.mainSidebar}>
                        <PenSize penSize={penSize} setPenSize={setPenSize} />
                        <MosaicSize
                            mosaicSize={mosaicSize}
                            setMosaicSize={setMosaicSize}
                            isAutoMosaicSize={isAutoMosaicSize}
                            setIsAutoMosaicSize={setIsAutoMosaicSize}
                            imageSize={imageSize}
                            />
                        <AiDetectionTools
                            frameCount={frameCount}
                            mosaicSize={mosaicSize}
                            isAutoMosaicSize={isAutoMosaicSize}
                            />
                        <div className={styles.divisionLine}></div>
                        <div>
                            <FileReset
                                setFrameCount={setFrameCount}
                                setFrameIndex={setFrameIndex}
                                setFrameImageUrl={setFrameImageUrl}
                                setImageSize={setImageSize}
                                setMaskCoordinates={setMaskCoordinates}
                                setisFileSelected={setisFileSelected}
                                />
                            <ExportButton isFileSelected={isFileSelected}/>
                        </div>
                    </div>
                    <div className={styles.mainCanvas}>
                        {isFileSelected
                            ? <div className={styles.CanvasWithNavigation}>
                                <KonvaCanvas
                                    // キャンバスを表示
                                    setFrameImageUrl={setFrameImageUrl}
                                    setMaskCoordinates={setMaskCoordinates}
                                    setImageSize={setImageSize}
                                    frameImageUrl={frameImageUrl}
                                    frameIndex={frameIndex}
                                    maskCoordinates={maskCoordinates}
                                    penSize={penSize}
                                    mosaicSize={mosaicSize}
                                    imageSize={imageSize}
                                    isMaskMode={isMaskMode}
                                    setCssCursorDeactive={setCssCursorDeactive}
                                    cssCursorDeactive={cssCursorDeactive}
                                    isProcessedImage={isProcessedImage}
                                    setIsProcessedImage={setIsProcessedImage}
                                    />
                                <div>
                                    <FrameNavigation
                                        // フレーム移動用ナビゲーション
                                        frameIndex={frameIndex}
                                        frameCount={frameCount}
                                        setFrameImageUrl={setFrameImageUrl}
                                        setFrameIndex={setFrameIndex}
                                        isMaskMode={isMaskMode}
                                        isMaskmodeButtonActive={isMaskmodeButtonActive}
                                        setIsQuickMaskMode={setIsQuickMaskMode}
                                        isQuickMaskMode={isQuickMaskMode}
                                        setIsMaskModeButtonActive={setIsMaskModeButtonActive}
                                        setIsProcessedImage={setIsProcessedImage}
                                        />
                                </div>
                            </div>
                            : <ChangeFile
                            // ファイルアップロード画面を表示
                                setFrameCount={setFrameCount}
                                setFrameIndex={setFrameIndex}
                                setFrameImageUrl={setFrameImageUrl}
                                setImageSize={setImageSize}
                                setisFileSelected={setisFileSelected}
                                />
                            }
                    </div>
                </div>
            </div>
        </>
    );
};

export default VideoEditor;
