// ペンのサイズを変えるためのコンポーネント
import React, { useCallback, useEffect } from 'react';
import styles from './style.module.sass';

const MIN_PEN_SIZE = 1;
const MAX_PEN_SIZE = 100;
const ADJUST_PEN_SIZE_STEP = 3;

type PenSizeProps = {
    penSize: number;
    setPenSize: React.Dispatch<React.SetStateAction<number>>;
};

export const PenSize: React.FC<PenSizeProps> = React.memo(({ penSize, setPenSize }) => {

    const handlePenSizeChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setPenSize(Number(event.target.value));
    }, [setPenSize]);

    const adjustPenSize = useCallback((adjustment: number) => {
        setPenSize(prevSize => Math.max(MIN_PEN_SIZE, Math.min(MAX_PEN_SIZE, prevSize + adjustment)));
    }, [setPenSize]);

    useEffect(() => {
        const handleKeyDown = (event: KeyboardEvent) => {
            if (event.key === '[') {
                adjustPenSize(-ADJUST_PEN_SIZE_STEP);
            }
            if (event.key === ']') {
                adjustPenSize(ADJUST_PEN_SIZE_STEP);
            }
        }
        window.addEventListener('keydown', handleKeyDown);
        return () => {
            window.removeEventListener('keydown', handleKeyDown);
        };
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div className={styles.penSize}>
            <div className={styles.navLabelStyle}>
                <label>Pen Size</label>
            </div>
            <div className={styles.rangeField}>
                <input
                    type="range"
                    min={MIN_PEN_SIZE}
                    max={MAX_PEN_SIZE}
                    value={penSize}
                    onChange={handlePenSizeChange}
                />
                <input type="number" value={penSize} onChange={handlePenSizeChange} />
            </div>
        </div>
    )
})