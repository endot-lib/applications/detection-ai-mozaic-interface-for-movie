// モザイクのサイズを変えるためのコンポーネント
import React, { useCallback } from 'react';
import styles from './style.module.sass';

const MIN_MOSAIC_SIZE = 1;
const MAX_MOSAIC_SIZE = 50;

type MosaciSizeProps = {
    mosaicSize: number;
    setMosaicSize: React.Dispatch<React.SetStateAction<number>>;
    isAutoMosaicSize: boolean;
    setIsAutoMosaicSize: React.Dispatch<React.SetStateAction<boolean>>;
    imageSize: { width: number; height: number };
};

export const MosaicSize: React.FC<MosaciSizeProps> = React.memo(({
    mosaicSize,
    setMosaicSize,
    isAutoMosaicSize,
    setIsAutoMosaicSize,
    imageSize
}) => {

    const handleMosaicSizeChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setMosaicSize(Number(event.target.value));
    }, [setMosaicSize]);

    // autoモードの場合はスライダーを無効化して計算済みの値を設定する
    const handleAutoMosaicSizeChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setIsAutoMosaicSize(event.target.checked);
        if (event.target.checked) {
            // 最小4ピクセル平方モザイクかつ画像全体の長辺が400ピクセル以上の場合、必要部位に「画像全体長辺*1/100」程度を算出
            if (imageSize.width === 0 || imageSize.height === 0) {
                return;
            }
            const maxMosaicSize = Math.max(imageSize.width, imageSize.height);
            if (maxMosaicSize >= 400) {
                setMosaicSize(Math.floor(maxMosaicSize / 100));
            } else {
                setMosaicSize(4);
            }
        }
    }, [setIsAutoMosaicSize, setMosaicSize, imageSize]);

    return (
        <div className={styles.penSize}>
            <div
                className={styles.navLabelStyle}
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingRight: '31px',
                }}
                >
                <label>Mosaic Size</label>
                <label style={{display: 'flex', alignItems: 'center'}}>
                    <div>Auto</div>
                    <div><input type="checkbox" checked={isAutoMosaicSize} onChange={handleAutoMosaicSizeChange} /></div>
                </label>
            </div>
            <div className={styles.rangeField}>
                <input
                    type="range"
                    min={MIN_MOSAIC_SIZE}
                    max={MAX_MOSAIC_SIZE}
                    value={mosaicSize}
                    onChange={handleMosaicSizeChange}
                    disabled={isAutoMosaicSize}
                />
                <input type="number" value={mosaicSize} onChange={handleMosaicSizeChange} disabled={isAutoMosaicSize} />
            </div>
        </div>
    )
})
