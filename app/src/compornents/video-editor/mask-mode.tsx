import React, { useEffect } from 'react';
import { GetFrameRequest } from '../../types/FlaskClientTypes';
import { fetchFrame } from '../../utils/fetch-frame';
import styles from './style.module.sass';
import { faCircleHalf } from '@fortawesome/pro-duotone-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { convertBase64ToImageUrl } from '../../utils/convert-base64-to-imageurl';


type MaskModeProps = {
    frameIndex: number;
    setFrameImageUrl: React.Dispatch<React.SetStateAction<string | null>>;
    // マスクモード関連
    isQuickMaskMode: boolean;
    isMaskmodeButtonActive: boolean;
    setIsQuickMaskMode: React.Dispatch<React.SetStateAction<boolean>>;
    setIsMaskModeButtonActive: React.Dispatch<React.SetStateAction<boolean>>;
};

export const MaskMode: React.FC<MaskModeProps> = React.memo(({
    frameIndex,
    setFrameImageUrl,
    // マスクモード関連
    isQuickMaskMode,
    isMaskmodeButtonActive,
    setIsQuickMaskMode,
    setIsMaskModeButtonActive,
}) => {

    const createFetchFrameRequest = (imageType: 'processed' | 'visualized'): GetFrameRequest => {
        return {
            frame_index: frameIndex,
            frame_type: imageType
        };
    }
    useEffect(() => {
        const handleKeyDown = async (e: KeyboardEvent) => {
            if ((e.key === 'q' || e.key === 'Q') && !isQuickMaskMode) {
                if (isMaskmodeButtonActive) return;
                setIsQuickMaskMode(true);
                // visualizedの画像を取得する
                const frameResponse = await fetchFrame(createFetchFrameRequest('visualized'));
                if (frameResponse.frame_image) {
                    setFrameImageUrl(convertBase64ToImageUrl(frameResponse.frame_image));
                }
            }
        };
        const handleKeyUp = async (e: KeyboardEvent) => {
            if ((e.key === 'q' || e.key === 'Q') && isQuickMaskMode){
                if (isMaskmodeButtonActive) return;
                setIsQuickMaskMode(false);
                // processedの画像を取得する（base64デコードしてセットする）
                const frameResponse = await fetchFrame(createFetchFrameRequest('processed'));
                if (frameResponse.frame_image) {
                    setFrameImageUrl(convertBase64ToImageUrl(frameResponse.frame_image));
                }
            }
        };
        window.addEventListener('keydown', handleKeyDown);
        window.addEventListener('keyup', handleKeyUp);
        return () => {
            window.removeEventListener('keydown', handleKeyDown);
            window.removeEventListener('keyup', handleKeyUp);
        };
    });

    // isMaskmodeButtonActiveがtrueになったら画像をマスクモード用に差し替える
    useEffect(() => {
        const fetchFrameRequest = createFetchFrameRequest(isMaskmodeButtonActive ? 'visualized' : 'processed');
        const fetchFrameUrl = async () => {
            const frameResponse = await fetchFrame(fetchFrameRequest);
            if (frameResponse.frame_image) {
                // base64デコードしてセットする
                setFrameImageUrl(convertBase64ToImageUrl(frameResponse.frame_image));
            }
        };
        fetchFrameUrl();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isMaskmodeButtonActive])

    return (
        <>
            <div
                onClick={() => {setIsMaskModeButtonActive(!isMaskmodeButtonActive)}}
                ><FontAwesomeIcon
                    icon={faCircleHalf}
                    className={isMaskmodeButtonActive || isQuickMaskMode
                        ? styles.activeMaskModeButton
                        : styles.maskModeButton
                    }
                    ></FontAwesomeIcon>
            </div>
        </>
    )
});