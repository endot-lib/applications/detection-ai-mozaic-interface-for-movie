// 編集中のファイルをエクスポートするボタン
import React, { useState, useEffect } from 'react';
import styles from './style.module.sass';
import io from 'socket.io-client';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDownload } from '@fortawesome/pro-solid-svg-icons';
import { faSpinnerScale } from '@fortawesome/pro-solid-svg-icons';
import { FlaskClient } from '../../utils/flask-client';

type Props = {
    isFileSelected: boolean;
};

export const ExportButton: React.FC<Props> = ({
    isFileSelected,
}) => {
    // エクスポート中かどうかを管理する変数
    const [isExporting, setIsExporting] = useState(false);
    // エクスポート中に進捗をサーバーから受け取って表示するための処理
    const [exportProgress, setExportProgress] = useState({
        frameCount: 0, // 総フレーム数
        currentFrameIndex: 0, // 現在のフレーム数
    });

    // エクスポート中の進捗をサーバーから受け取るための処理
    useEffect(() => {
        // package.jsonのproxy設定により、http://localhost:5000に接続される
        const socket = io(); // socket.io-clientのデフォルトの接続先はhttp://localhost:5000
        socket.on('exportProgress', data => {
            setExportProgress(data);
        });
        return () => {
            socket.disconnect();
        };
    }, []);

    // 動画ファイルをダウンロードする関数
    const downloadFile = async (filename: string) => {
        const fileData = await FlaskClient.getOutputFile(filename);
        const url = window.URL.createObjectURL(new Blob([fileData], { type: 'video/mp4' }));
        const link = document.createElement('a');
        link.href = url;
        link.download = filename;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        window.URL.revokeObjectURL(url);
        setIsExporting(false);
    };

    // エクスポート処理を行う関数
    const handleExport = async () => {
        if (isExporting) return; // エクスポート中は何もしない
        try {
            setIsExporting(true);
            const results = await FlaskClient.exportVideo();
            if (results.data) {
                await downloadFile(results.data);
            }
            setIsExporting(false);
        } catch (error) {
            console.error('Export error:', error);
        }
    };

    return (
        <div className={styles.fileResetButton}>
            <button
                onClick={handleExport}
                disabled={isExporting || !isFileSelected}
                className={isExporting ? styles.blinkEffect : ''}
                >
                {isExporting
                    // 進捗％を表示する
                    ? <>
                        <FontAwesomeIcon icon={faSpinnerScale} className='fa-fw' spinPulse />
                        Exporting now...{(
                            (exportProgress.currentFrameIndex / exportProgress.frameCount) * 100
                        ).toFixed(0)}%
                      </>
                    : <>Export<FontAwesomeIcon icon={faDownload} /></>
                }
            </button>
        </div>
    );
};
