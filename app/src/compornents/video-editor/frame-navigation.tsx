import React, { useCallback, useEffect } from 'react';
import { FlaskClient } from '../../utils/flask-client';
import styles from './style.module.sass';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { TimelineThumbnails } from './timeline-thumbnails';
import { GetFrameRequest, ResetConvertImageRequest } from '../../types/FlaskClientTypes';
import { fetchFrame } from '../../utils/fetch-frame';
import { MaskMode } from './mask-mode';
import { convertBase64ToImageUrl } from '../../utils/convert-base64-to-imageurl';

type FrameNavigationProps = {
    frameIndex: number;
    frameCount: number;
    setFrameImageUrl: React.Dispatch<React.SetStateAction<string | null>>;
    setFrameIndex: React.Dispatch<React.SetStateAction<number>>;
    // マスクモード関連
    isMaskMode: boolean;
    isQuickMaskMode: boolean;
    isMaskmodeButtonActive: boolean;
    setIsQuickMaskMode: React.Dispatch<React.SetStateAction<boolean>>;
    setIsMaskModeButtonActive: React.Dispatch<React.SetStateAction<boolean>>;
    setIsProcessedImage: React.Dispatch<React.SetStateAction<boolean>>;
};

export const FrameNavigation: React.FC<FrameNavigationProps> = ({
    frameIndex,
    frameCount,
    setFrameImageUrl,
    setFrameIndex,
    // マスクモード関連
    isMaskMode,
    isQuickMaskMode,
    isMaskmodeButtonActive,
    setIsQuickMaskMode,
    setIsMaskModeButtonActive,
    setIsProcessedImage,
}) => {

    // 前のフレームまたは次のフレームへのナビゲーション
    const _navigateFrame = useCallback(async (direction: 'next' | 'previous') => {
        let newIndex = direction === 'next' ? frameIndex + 1 : frameIndex - 1;
        // マウスカーソルを非アクティブにする
        // 範囲を超えそうな場合はバックエンドと通信せずに終了
        if (newIndex < 0 || newIndex >= frameCount) return;
        newIndex = Math.max(0, Math.min(newIndex, frameCount - 1)); // 範囲を超えないように制限
        const fetchFrameRequest: GetFrameRequest = {
            frame_index: newIndex,
            frame_type: isMaskMode ? 'visualized' : 'processed'
        };
        const frameResponse = await fetchFrame(fetchFrameRequest);
        if (frameResponse.frame_image) {
            setFrameImageUrl(convertBase64ToImageUrl(frameResponse.frame_image));
            setFrameIndex(newIndex);
            setIsProcessedImage(frameResponse.is_processed)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [frameIndex, frameCount, setFrameImageUrl, setFrameIndex, isMaskMode]);

    // 前のフレームへ移動する
    const handlePreviousFrame = useCallback(() => {
        _navigateFrame('previous');
    }, [_navigateFrame]);
    // 次のフレームへ移動する
    const handleNextFrame = useCallback(() => {
        _navigateFrame('next');
    }, [_navigateFrame]);
    // キーボードの左右キーでフレームを移動する
    useEffect(() => {
        const handleKeyPress = (e: KeyboardEvent) => {
            if (e.key === 'a' || e.key === 'A' || e.key === 'ArrowLeft') {
                handlePreviousFrame();
            } else if (e.key === 'd' || e.key === 'D' || e.key === 'ArrowRight') {
                handleNextFrame();
            }
        };
        window.addEventListener('keydown', handleKeyPress);
        return () => {
            window.removeEventListener('keydown', handleKeyPress);
        };
    }, [handlePreviousFrame, handleNextFrame]);

    // モザイクをリセットするボタン
    const frameMosaicRemove = async () => {
        try {
            const resetConvertImageRequest: ResetConvertImageRequest = {
                frame_index: frameIndex,
            };
            const getFrameResponse = await FlaskClient.resetConvertImage(resetConvertImageRequest);
            if (getFrameResponse.data.frame_image) {
                setFrameImageUrl(convertBase64ToImageUrl(getFrameResponse.data.frame_image));
                setIsProcessedImage(getFrameResponse.data.is_processed)
            }
        } catch (error) {
            console.error('Error fetching original frame:', error);
            setFrameImageUrl(null);
            setIsProcessedImage(false);
        }
    };

    return (
        <>
            <div className={styles.frameTools}>
                <div className={styles.frameMosaicRemoveButton}>
                    <button
                        onClick={frameMosaicRemove}
                        >Mask Remove</button>
                </div>
                <div className={styles.frameIndexCounter}>
                    <p>Frame {frameIndex + 1} of {frameCount}</p>
                </div>
                <MaskMode
                    frameIndex={frameIndex}
                    setFrameImageUrl={setFrameImageUrl}
                    isQuickMaskMode={isQuickMaskMode}
                    isMaskmodeButtonActive={isMaskmodeButtonActive}
                    setIsQuickMaskMode={setIsQuickMaskMode}
                    setIsMaskModeButtonActive={setIsMaskModeButtonActive}
                    />
            </div>
            <>
                <div className={styles.FrameNavigation}>
                    <>
                        <button
                            onClick={handlePreviousFrame}
                            className={styles.navigationButton}
                        ><FontAwesomeIcon icon={faChevronLeft} /></button>
                    </>
                    <TimelineThumbnails
                        frameCount={frameCount}
                        setFrameIndex={setFrameIndex}
                        setFrameImageUrl={setFrameImageUrl}
                        isMaskMode={isMaskMode}
                        setIsProcessedImage={setIsProcessedImage}
                    />
                    <>
                        <button
                            onClick={handleNextFrame}
                            className={styles.navigationButton}
                        ><FontAwesomeIcon icon={faChevronRight} /></button>
                    </>
                </div>
            </>
        </>
    );
};

export default FrameNavigation;
