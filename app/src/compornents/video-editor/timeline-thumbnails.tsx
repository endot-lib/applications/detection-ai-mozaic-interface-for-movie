import React, { useEffect, useState } from 'react';
import { FlaskClient } from '../../utils/flask-client';
import styles from './style.module.sass';
import { useSettings } from '../../SettingsContext';
import { GetFramesRequest, GetFrameRequest } from '../../types/FlaskClientTypes'
import { fetchFrame } from '../../utils/fetch-frame';
import { convertBase64ToImageUrl } from '../../utils/convert-base64-to-imageurl';

type TimelineThumbnailsProps = {
    frameCount: number,
    setFrameIndex: React.Dispatch<React.SetStateAction<number>>;
    setFrameImageUrl: React.Dispatch<React.SetStateAction<string | null>>;
    isMaskMode: boolean;
    setIsProcessedImage: React.Dispatch<React.SetStateAction<boolean>>;
};

export const TimelineThumbnails: React.FC<TimelineThumbnailsProps> = React.memo(({
    frameCount,
    setFrameIndex,
    setFrameImageUrl,
    isMaskMode,
    setIsProcessedImage,
}) => {
    const { timelineThumbnailNum } = useSettings();
    const [thumbnails, setThumbnails] = useState<string[]>([]);
    // frame_index_intervalの計算
    const calculateFrameIndexInterval = (totalFrames: number): number => {
        return totalFrames < timelineThumbnailNum ? 1 : Math.ceil(totalFrames / timelineThumbnailNum);
    };
    const frameIndexInterval = calculateFrameIndexInterval(frameCount);

    useEffect(() => {
        const fetchThumbnails = async () => {
            try {
                const request: GetFramesRequest = {
                    total_frame_index: frameCount,
                    frame_index_interval: frameIndexInterval,
                };
                const timelineThumbnails = await FlaskClient.getTimelineThumbnails(request);
                const newThumbnails = timelineThumbnails.data.frame_images
                    .map((thumbnail: any) => thumbnail)
                    .filter((thumbnail: any) => thumbnail !== null)
                    .map((encodedImage: any) => `data:image/jpeg;base64,${encodedImage}`);
                setThumbnails(newThumbnails);

            } catch (error) {
                console.error('Error fetching thumbnails:', error);
            }
        };
        fetchThumbnails();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleThumbnailClick = async (index: number) => {
        const frameIndex = index * frameIndexInterval;
        setFrameIndex(frameIndex);
        const fetchFrameRequest: GetFrameRequest = {
            frame_index: frameIndex,
            frame_type: isMaskMode ? 'visualized' : 'processed'
        };
        const frameResponse = await fetchFrame(fetchFrameRequest);
        if (frameResponse.frame_image) {
            setFrameImageUrl(convertBase64ToImageUrl(frameResponse.frame_image));
            setIsProcessedImage(frameResponse.is_processed)
        }
    };

    return (
        <div className={styles.timelineThumbnails}>
            {thumbnails.map((thumbnail, index) => (
                <img
                    key={index}
                    src={thumbnail}
                    alt={`Frame ${index * calculateFrameIndexInterval(frameCount)}`}
                    onClick={() => handleThumbnailClick(index)}
                />
            ))}
        </div>
    )
});
