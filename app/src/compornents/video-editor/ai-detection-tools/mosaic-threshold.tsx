// モザイクスレッショルドを指定するコンポーネント
import React, { useEffect } from 'react';
import styles from '../style.module.sass';

type MosaicThresholdProps = {
    mosaicThreshold: number;
    setMosaicThreshold: (mosaicThreshold: number) => void;
}

export const MosaicThreshold: React.FC<MosaicThresholdProps> = ({
    mosaicThreshold, setMosaicThreshold,
}) => {
    useEffect(() => {
        setMosaicThreshold(0.2);
    }, [setMosaicThreshold]);

    return (
        <>
            <div className={styles.penSize}>
                <div className={styles.navLabelStyle}>
                    <label>Mosaic Threshold</label>
                </div>
                <div className={styles.rangeField}>
                    <input
                        type="range"
                        value={mosaicThreshold}
                        min={0}
                        max={1}
                        step={0.01}
                        onChange={e => setMosaicThreshold(parseFloat(e.target.value))}
                        />
                    <input
                        type="number"
                        value={mosaicThreshold}
                        onChange={e => setMosaicThreshold(parseInt(e.target.value))}
                        />
                </div>
            </div>
        </>
    )
}