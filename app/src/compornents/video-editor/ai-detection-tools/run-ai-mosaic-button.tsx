// AIによる物体検出処理を実行するボタン
import React, { useEffect } from 'react';
import io from 'socket.io-client';
import styles from '../style.module.sass';
import { FlaskClient } from '../../../utils/flask-client';
import { RunAIProgressObject } from '../../../types/FlaskClientTypes';

type RunAIMosaicButtonProps = {
    frameRange: [number, number];
    mosaicExpandSize: number;
    mosaicThreshold: number;
    mosaicSize: number;
    isAutoMosaicSize: boolean;
    selectedDetectModel: string;
    selectedSegmentModel: string;
    isAIMosaicProcessing: boolean;
    aiDetectionProgressObject: RunAIProgressObject;
    setAiDetectionProgressObject: React.Dispatch<React.SetStateAction<RunAIProgressObject>>;
    setIsAIMosaicProcessing: React.Dispatch<React.SetStateAction<boolean>>;
}

export const RunAIMosaicButton: React.FC<RunAIMosaicButtonProps> = ({
    frameRange,
    mosaicExpandSize,
    mosaicThreshold,
    mosaicSize,
    isAutoMosaicSize,
    selectedDetectModel,
    selectedSegmentModel,
    isAIMosaicProcessing, setIsAIMosaicProcessing,
    aiDetectionProgressObject, setAiDetectionProgressObject,

}) => {

    useEffect(() => {
        const socket = io();
        socket.on('runAIProgress', data => {
            setAiDetectionProgressObject(data);
        });
        return () => {
            socket.disconnect();
        };
    }, [setAiDetectionProgressObject])

    const runAiMosaic = async () => {
        try {
            setIsAIMosaicProcessing(true);
            // AIによるモザイク処理を実行する処理
            await FlaskClient.runAutoDetection({
                frameRange,
                mosaicExpandSize,
                mosaicThreshold,
                mosaicSize,
                isAutoMosaicSize,
                selectedDetectModel,
                selectedSegmentModel,
            });
        } catch (error) {
            console.error('Error running AI Mosaic:', error);
        } finally {
            setIsAIMosaicProcessing(false);
        }
    };
    return (
        <>
            <div className={styles.fileResetButton}>
                <button
                    onClick={runAiMosaic}
                    className={isAIMosaicProcessing ? styles.blinkEffect : ''}
                    >
                {!isAIMosaicProcessing ? (
                    <div>Run AI Mosaic</div>
                ) : (
                    <div>
                        <div>{aiDetectionProgressObject.message}</div>
                        <div>{aiDetectionProgressObject.current_num} / {aiDetectionProgressObject.total_num} frames</div>
                    </div>
                )}
                </button>
            </div>
        </>
    )
}