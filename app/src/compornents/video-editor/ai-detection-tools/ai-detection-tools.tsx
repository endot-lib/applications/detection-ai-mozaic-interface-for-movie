// AIによる物体検出に関連するコンポーネントをまとめる
import React, { useEffect } from 'react';
import styles from '../style.module.sass';
import { ModelSelector } from './model-selector';
import { TargetFrameRange } from './target-frame-range';
import { RunAIProgressObject } from '../../../types/FlaskClientTypes';
import { MosaicExpandSize } from './mosaic-expand-size';
import { MosaicThreshold } from './mosaic-threshold';
import { RunAIMosaicButton } from './run-ai-mosaic-button';

type AiDetectionToolsProps = {
    frameCount: number;
    mosaicSize: number;
    isAutoMosaicSize: boolean;
}
export const AiDetectionTools: React.FC<AiDetectionToolsProps> = ({
    frameCount,
    mosaicSize,
    isAutoMosaicSize,
}) => {
    // ツールを表示すべきかどうかを保持するステート
    const [isToolVisible, setIsToolVisible] = React.useState<boolean>(false);
    // AIによる物体検出の範囲となるフレームレンジ（スタートと終わり）を保持するステート
    const [frameRange, setFrameRange] = React.useState<[number, number]>([1, frameCount]);
    // 検出範囲からどのていど拡大してモザイクを掛けるかを指定するステート
    const [mosaicExpandSize, setMosaicExpandSize] = React.useState<number>(0);
    // 検出するしきい値を指定するステート
    const [mosaicThreshold, setMosaicThreshold] = React.useState<number>(0.2); // 0-1の範囲で指定、1に近いほど厳密になり検出しにくい
    // AIによりモザイク処理中かどうかを保持するステート
    const [isAIMosaicProcessing, setIsAIMosaicProcessing] = React.useState<boolean>(false);
    // 選択中のdetectモデルを保持するステート
    const [selectedDetectModel, setSelectedDetectModel] = React.useState<string>('');
    // 選択中のsegmentモデルを保持するステート
    const [selectedSegmentModel, setSelectedSegmentModel] = React.useState<string>('');
    // AIで物体検出している進捗を追跡するためのステート
    const [aiDetectionProgressObject, setAiDetectionProgressObject] = React.useState<RunAIProgressObject>({
        message: '',
        total_num: 0,
        current_num: 0,
    });

    // ツールを表示すべきかどうかを判定する
    useEffect(() => {
        setIsToolVisible(frameCount !== 1);
    }, [frameCount]);

    return (
        <>
            {isToolVisible &&(
                <div className={styles.toolVisible}>
                    <ModelSelector
                        setSelectedDetectModel={setSelectedDetectModel}
                        setSelectedSegmentModel={setSelectedSegmentModel}
                        />
                    <TargetFrameRange
                        frameCount={frameCount}
                        setFrameRange={setFrameRange}
                        frameRange={frameRange}
                        />
                    <MosaicExpandSize
                        mosaicExpandSize={mosaicExpandSize}
                        setMosaicExpandSize={setMosaicExpandSize}
                        />
                    <MosaicThreshold
                        mosaicThreshold={mosaicThreshold}
                        setMosaicThreshold={setMosaicThreshold}
                        />
                    <RunAIMosaicButton
                        frameRange={frameRange}
                        mosaicExpandSize={mosaicExpandSize}
                        mosaicThreshold={mosaicThreshold}
                        mosaicSize={mosaicSize}
                        isAutoMosaicSize={isAutoMosaicSize}
                        selectedDetectModel={selectedDetectModel}
                        selectedSegmentModel={selectedSegmentModel}
                        isAIMosaicProcessing={isAIMosaicProcessing}
                        aiDetectionProgressObject={aiDetectionProgressObject}
                        setAiDetectionProgressObject={setAiDetectionProgressObject}
                        setIsAIMosaicProcessing={setIsAIMosaicProcessing}
                        />
                </div>
            )}
        </>
    );
}