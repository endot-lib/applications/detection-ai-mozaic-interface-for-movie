// AIによる物体検出の対象となるフレーム範囲を指定するコンポーネント
import React, { useEffect } from 'react';
import styles from '../style.module.sass';

type TargetFrameRangeProps = {
    frameCount: number;
    setFrameRange: React.Dispatch<React.SetStateAction<[number, number]>>;
    frameRange: [number, number];
}

export const TargetFrameRange: React.FC<TargetFrameRangeProps> = ({
    frameCount,
    frameRange, setFrameRange,
}) => {

    useEffect(() => {
        // フレーム数が変更されたら、フレーム範囲をリセットする
        setFrameRange([1, frameCount]);
    }, [frameCount, setFrameRange]); // frameRangeを依存関係から削除

    return (
        <>
            <div className={styles.targetFrameRange}>
                <h3>AI Detection Target Frame Range</h3>
                <div className={styles.targetFrameRangeChildren}>
                    <div>
                        <label>Start Frame:</label>
                        <input
                            type="number"
                            value={frameRange[0]}
                            min={1}
                            max={frameCount}
                            onChange={e => setFrameRange([parseInt(e.target.value), frameRange[1]])}
                        />
                    </div>
                    <div></div>
                    <div>
                        <label>End Frame:</label>
                        <input
                            type="number"
                            value={frameRange[1]}
                            min={1}
                            max={frameCount}
                            onChange={e => setFrameRange([frameRange[0], parseInt(e.target.value)])}
                        />
                    </div>
                </div>
            </div>
        </>
    );
}