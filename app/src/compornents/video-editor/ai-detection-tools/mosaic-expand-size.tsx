// モザイクサイズを指定するコンポーネント
import React, { useEffect } from 'react';
import styles from '../style.module.sass';

type MosaicExpandSizeProps = {
    mosaicExpandSize: number;
    setMosaicExpandSize: (mosaicExpandSize: number) => void;
}

export const MosaicExpandSize: React.FC<MosaicExpandSizeProps> = ({
    mosaicExpandSize, setMosaicExpandSize,
}) => {
    useEffect(() => {
        setMosaicExpandSize(0);
    }, [setMosaicExpandSize]);

    return (
        <>
            <div className={styles.penSize}>
                <div className={styles.navLabelStyle}>
                    <label>Mosaic Expand Size (%)</label>
                </div>
                <div className={styles.rangeField}>
                    <input
                        type="range"
                        value={mosaicExpandSize}
                        min={0}
                        max={100}
                        step={1}
                        onChange={e => setMosaicExpandSize(parseInt(e.target.value))}
                        />
                    <input
                        type="number"
                        value={mosaicExpandSize}
                        onChange={e => setMosaicExpandSize(parseInt(e.target.value))}
                        />
                </div>
            </div>
        </>
    )
}