import React, { useState, useEffect } from 'react';
import styles from '../style.module.sass';
import { FlaskClient } from '../../../utils/flask-client';
import { ModelListResponse } from '../../../types/FlaskClientTypes';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowsRotate } from '@fortawesome/free-solid-svg-icons';

type ModelSelectorProps = {
    setSelectedDetectModel: React.Dispatch<React.SetStateAction<string>>;
    setSelectedSegmentModel: React.Dispatch<React.SetStateAction<string>>;
}

export const ModelSelector: React.FC<ModelSelectorProps> = ({
    setSelectedDetectModel,
    setSelectedSegmentModel,
}) => {
    const [modelList, setModelList] = useState<ModelListResponse['models']>({
         detect_models: [],
         segment_models: []
    });

    // モデルリストを取得する関数
    const fetchModelList = () => {
        FlaskClient.getModelList()
            .then(response => {
                setModelList(response.data.models);
                setSelectedDetectModel(response.data.models.detect_models[0]);
                setSelectedSegmentModel(response.data.models.segment_models[0]);
            })
            .catch(error => {
                console.error("Failed to fetch model list:", error);
            });
    };

    useEffect(() => {
        fetchModelList(); // コンポーネントマウント時にモデルリストを取得
        // eslint-disable-next-line
    }, []);

    return (
        <>
            <div className={styles.modelSelector}>
                <div>
                    <h3>Detect Models</h3>
                    {/* selectedなモデルが変わったら更新する */}
                    <select
                        onChange={(e) => setSelectedDetectModel(e.target.value)}
                        >
                        {modelList.detect_models?.map((model, index) => (
                            <option key={index}>{model}</option>
                        ))}
                    </select>
                </div>
                <div>
                    <h3>Segment Models</h3>
                    <select onChange={(e) => setSelectedSegmentModel(e.target.value)}>
                        {modelList.segment_models?.map((model, index) => (
                            <option key={index}>{model}</option>
                            ))}
                    </select>
                </div>
                <button onClick={fetchModelList}>Update Model List<FontAwesomeIcon icon={faArrowsRotate} /></button>
            </div>
        </>
    );
};
