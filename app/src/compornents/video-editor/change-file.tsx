import { useState, useRef, useEffect } from 'react';
import { FlaskClient } from '../../utils/flask-client';
import { adjustImageSize } from '../../utils/adjust-image-size'
import { useSettings } from '../../SettingsContext';
import styles from './style.module.sass';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUpFromBracket } from '@fortawesome/free-solid-svg-icons';
import io from 'socket.io-client';
import { memo } from 'react';
import {
    FileUploadRequest,
    FileUploadProgressObject
} from '../../types/FlaskClientTypes';
import { convertBase64ToImageUrl } from '../../utils/convert-base64-to-imageurl';

type ChangeFileProps = {
    setFrameCount: React.Dispatch<React.SetStateAction<number>>;
    setFrameIndex: React.Dispatch<React.SetStateAction<number>>;
    setFrameImageUrl: React.Dispatch<React.SetStateAction<string | null>>;
    setImageSize: React.Dispatch<React.SetStateAction<{ width: number; height: number }>>;
    setisFileSelected: React.Dispatch<React.SetStateAction<boolean>>;
};

export const ChangeFile: React.FC<ChangeFileProps> = memo(({
    setFrameCount,
    setFrameIndex,
    setFrameImageUrl,
    setImageSize,
    setisFileSelected,
}) => {
    const { maxWidth, maxHeight } = useSettings();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    // ファイルアップロード中の進捗を追跡するためのステート
    const [fileUploadProgressObject, setFileUploadProgressObject] = useState<FileUploadProgressObject>({
        message: ''
    });

    // ファイルアップロード中の進捗をサーバーから受け取るための処理
    useEffect(() => {
        // package.jsonのproxy設定により、http://localhost:5000に接続される
        const socket = io(); // socket.io-clientのデフォルトの接続先はhttp://localhost:5000
        socket.on('uploadProgress', data => {
            setFileUploadProgressObject(data);
        });
        return () => {
            socket.disconnect();
        };
    }, []);

    // ファイルが選択されたかドロップされた時の共通処理
    const handleFile = async (file: File) => {
        setIsLoading(true);  // アップロード開始時にローディング状態に設定
        await FlaskClient.reset(); // 初期化処理
        const fileUploadRequest: FileUploadRequest = new FormData();
        fileUploadRequest.append('video', file);
        try {
            const fileUploadResponse = await FlaskClient.fileUpload(fileUploadRequest);
            setFrameCount(fileUploadResponse.data.frame_count);
            setFrameIndex(0);
            setFrameImageUrl(convertBase64ToImageUrl(fileUploadResponse.data.frame_image));
            setImageSize(adjustImageSize(
                fileUploadResponse.data.resized_width,
                fileUploadResponse.data.resized_height,
                maxWidth,
                maxHeight
                ));
            setisFileSelected(true);
        } catch (error) {
            console.error('Error uploading file:', error);
        }
        setIsLoading(false);  // アップロード終了時にローディング状態を解除
    };

    const onFileChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.files) {
            await handleFile(event.target.files[0]);
        }
    };

    const onDrop = async (event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
        if (event.dataTransfer.files) {
            await handleFile(event.dataTransfer.files[0]);
        }
    };

    const fileInputRef = useRef<HTMLInputElement>(null);

    const onDropAreaClick = () => {
        if (isLoading) {
            return;
        }
        fileInputRef.current?.click();
    };

    return (
        <div
            onDrop={onDrop}
            onDragOver={(event) => event.preventDefault()}
            onClick={onDropAreaClick} // ドロップエリアをクリックしたときの処理
            className={styles.onChangeFileDroparea}
            style={{
                height: '100%',
            }}
        >
            <input
                type="file"
                accept="video/*, image/gif"
                onChange={onFileChange}
                ref={fileInputRef}
                style={{ display: 'none' }} // ファイルインプットを非表示にする
            />
            {isLoading
                ? <div className={styles.fileUploadingProgressInfo}>
                    <div>{fileUploadProgressObject.message}</div>
                  </div>
                : <div className={styles.fileDropPlaceholder}>
                        <div><FontAwesomeIcon icon={faArrowUpFromBracket} /></div>
                        <div>ここに動画ファイルをドロップ</div>
                    </div>
            }
        </div>
    );
});
