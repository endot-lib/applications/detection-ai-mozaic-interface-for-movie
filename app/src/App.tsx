import React, { useEffect } from 'react';
import './App.css';
import { VideoEditor } from './compornents/video-editor';
import { Header } from './compornents/header';
import { Footer } from './compornents/footer';
import emotionReset from 'emotion-reset';
import { Global, css } from '@emotion/react';
import styles from './App.module.sass';
import { SettingsProvider } from './SettingsContext';

function App() {
  useEffect(() => {
    // ブラウザバックを無効化
    window.history.pushState(null, document.title, window.location.href);
    window.addEventListener('popstate', (event) => {
        window.history.pushState(null, document.title, window.location.href);
    });
}, []);
  return (
    <>
    <Global styles={css `${emotionReset}`}/>
      <div className={styles.App}>
        <SettingsProvider>
          <Header/>
          <VideoEditor />
          <Footer/>
        </SettingsProvider>
      </div>
    </>
  );
}

export default App;
