import React, { createContext, useContext, useState, useEffect, ReactNode } from 'react';
import { FlaskClient } from './utils/flask-client';

// 設定値の型を定義
type Settings = {
  maxWidth: number;
  maxHeight: number;
  timelineThumbnailNum: number;
};

// コンテキストの作成
const SettingsContext = createContext<Settings>({} as Settings); // デフォルト値なしで型アサーションを使用

interface SettingsProviderProps {
  children: ReactNode;
}

// Providerコンポーネントの作成
export const SettingsProvider: React.FC<SettingsProviderProps> = ({ children }) => {
  const [settings, setSettings] = useState<Settings>({
    maxWidth: 0, // 初期値。ただし、実際にはすぐに上書きされるため、この値は使用されない。
    maxHeight: 0,
    timelineThumbnailNum: 0,
  });

  useEffect(() => {
    // ここでサーバーから設定値をフェッチし、存在が保証されているためエラーハンドリングは省略
    FlaskClient.getSettings().then((response) => {
      setSettings({
        maxWidth: response.data.maxSize,
        maxHeight: response.data.maxSize,
        timelineThumbnailNum: response.data.timelineThumbnailNum,
      });
    });
  }, []);

  return (
    <SettingsContext.Provider value={settings}>
      {children}
    </SettingsContext.Provider>
  );
};

// コンテキストを使用するためのカスタムフック
export const useSettings = () => useContext(SettingsContext);
