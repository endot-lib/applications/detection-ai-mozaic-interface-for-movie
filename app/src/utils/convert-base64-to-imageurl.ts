// base64デコードして画像URLに変換する
export const convertBase64ToImageUrl = (base64String: string): string => {
    return `data:image/jpeg;base64,${base64String}`;
};