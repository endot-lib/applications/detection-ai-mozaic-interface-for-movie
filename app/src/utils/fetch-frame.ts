// この中間的な関数は消して良さそう
import { GetFrameRequest, GetFrameResponse } from "../types/FlaskClientTypes";
import { FlaskClient } from "./flask-client";

export const fetchFrame = async (frame_index: GetFrameRequest): Promise<GetFrameResponse> => {
    const getFrameResponse = await FlaskClient.getFrame(frame_index);
    return getFrameResponse.data;
};