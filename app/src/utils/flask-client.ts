import axios, { AxiosResponse } from 'axios';
import {
  FileUploadRequest, FileUploadResponse,
  GetFrameRequest, GetFrameResponse,
  GetFramesRequest, GetFramesResponse,
  ResetResponse,
  ConvertImageRequest, ConvertImageResponse,
  ResetConvertImageRequest,
  ModelListResponse,
  RunAutoDetectionRequest, RunAutoDetectionResponse,
 } from '../types/FlaskClientTypes';

export class FlaskClient {

  static async getSettings(): Promise<AxiosResponse<any>> {
    try {
      return await axios.post('settings');
    } catch (error) {
      console.error('Error in POST request:', error);
      throw error;
    }
  }
  // ファイルをアップロードするエンドポイント
  static async fileUpload(request: FileUploadRequest): Promise<AxiosResponse<FileUploadResponse>> {
    try {
      return await axios.post<FileUploadResponse>('file_upload', request, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
    } catch (error) {
      console.error('Error in POST request:', error);
      throw error;
    }
  }

  // frame_indexを指定して該当のフレームを取得する処理（frame_indexは数値）
  static async getFrame(request: GetFrameRequest): Promise<AxiosResponse<GetFrameResponse>> {
    try {
      return await axios.post<GetFrameResponse>(`frame/${request.frame_index}/image_type/${request.frame_type}`);
    } catch (error) {
      console.error('Error in GET request:', error);
      throw error;
    }
  }

  // タイムラインに使用する画像一覧を取得する処理
  static async getTimelineThumbnails(request: GetFramesRequest): Promise<AxiosResponse<GetFramesResponse>> {
    try {
      const frameIndexs = Array.from(
        { length: Math.ceil(request.total_frame_index / request.frame_index_interval) },
        (_, i) => i * request.frame_index_interval
      );
      return await axios.post<GetFramesResponse>('frames', {frame_indexs: frameIndexs});
    } catch (error) {
      console.error('Error in GET request:', error);
      throw error;
    }
  }

  // 読み込んだ動画データのリセット処理
  static async reset(): Promise<AxiosResponse<ResetResponse>> {
    try {
      return await axios.get<ResetResponse>('reset');
    } catch (error) {
      console.error('Error in GET request:', error);
      throw error;
    }
  }

  // 選択した座標領域にモザイクしてもらう処理
  static async convertImage(request: ConvertImageRequest): Promise<AxiosResponse<ConvertImageResponse>> {
    try {
      return await axios.post<ConvertImageResponse>('convert_image', request);
    } catch (error) {
      console.error('Error in POST request:', error);
      throw error;
    }
  }

  // 指定されたフレーム画像のモザイクをリセットする処理（元画像に戻す）
  static async resetConvertImage(request: ResetConvertImageRequest): Promise<AxiosResponse<GetFrameResponse>> {
    try {
      return await axios.post<GetFrameResponse>('reset_convert_image', request);
    } catch (error) {
      console.error('Error in POST request:', error);
      throw error;
    }
  }

  // 編集した動画をエクスポートするリクエストを送信する処理
  static async exportVideo(): Promise<AxiosResponse<string>> {
    try {
      // 生成された動画のURLを受け取る
      return await axios.post<string>('export_video');
    } catch (error) {
      console.error('Error in POST request:', error);
      throw error;
    }
  }

  // バックエンドのoutputsディレクトリ内の静的なファイルを取得する
  static async getOutputFile(filename: string): Promise<Blob> {
    try {
      const response = await axios.post(`outputs/${filename}`, {}, { responseType: 'blob' });
      return response.data;
    } catch (error) {
      console.error('Error in POST request:', error);
      throw error;
    }
  }

  // バックエンドからモデルのリストを取得する処理
  static async getModelList(): Promise<AxiosResponse<ModelListResponse>> {
    try {
      return await axios.post<ModelListResponse>('models');
    } catch (error) {
      console.error('Error in POST request:', error);
      throw error;
    }
  }

  // バクエンドへAIによるモザイク処理をリクエストする処理
  static async runAutoDetection(request: RunAutoDetectionRequest): Promise<AxiosResponse<RunAutoDetectionResponse>> {
    try {
      return await axios.post<RunAutoDetectionResponse>('run_auto_detection', request);
    } catch (error) {
      console.error('Error in POST request:', error);
      throw error;
    }
  }
}
