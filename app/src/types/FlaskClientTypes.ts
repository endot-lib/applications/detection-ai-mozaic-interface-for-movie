// 型定義

export type Coordinate = {
    x: number;
    y: number;
};

// 定数
export type FileUploadRequest = FormData;
export type FileUploadResponse = {
    frame_count: number;
    frame_image: string,
    resized_width: number;
    resized_height: number;
}

type FrameType = "processed" | "visualized";

export type GetFrameRequest = {
    frame_index: number;
    frame_type: FrameType;
}
export type GetFrameResponse = {
    frame_index: number;
    frame_type: FrameType;
    frame_image: string | null;
    is_processed: boolean;
}

export type GetFramesRequest = {
    total_frame_index: number;
    frame_index_interval: number;
}
export type GetFramesResponse = {
    frame_images: string[];
}

export type FileUploadProgressObject = {
    message: string;
};

export type ResetResponse = {
    message: string;
}

export type ConvertImageRequest = {
    frame_index: number;
    pen_size: number;
    coordinates: Coordinate[];
    mosaic_size: number;
    frame_type: string; // "processed" | "visualized"
}

export type ConvertImageResponse = {
    message: string;
    convert_image: string | null;
    is_processed: boolean;
};

export type ResetConvertImageRequest = {
    frame_index: number;
}

export type ModelListResponse = {
    models: {
        detect_models: string[];
        segment_models: string[];
    }
}

export type RunAutoDetectionRequest = {
    frameRange: [number, number];
    mosaicExpandSize: number;
    mosaicThreshold: number;
    mosaicSize: number;
    isAutoMosaicSize: boolean;
    selectedDetectModel: string;
    selectedSegmentModel: string;
    // TODO [ ] すでにモザイク情報があるフレームを無視するかどうかの設定もあっていいかも？
}

export type RunAutoDetectionResponse = {
    message: string;
}

export type RunAIProgressObject = {
    message: string;
    total_num: number;
    current_num: number;
};
