#!/bin/bash

# 引数からMAX_SIZEを取得、指定されていない場合はデフォルト値を設定
MAX_SIZE=${1:-640}
TIMELINE_THUMBNAIL_NUM=${2:-100}

# 仮想環境のディレクトリパス
venvDir="venv"

# 仮想環境が存在しない場合は作成
if [ ! -d "$venvDir" ]; then
    python3 -m venv "$venvDir"
fi

# 仮想環境をアクティベート
source "$venvDir/bin/activate"

# 必要なパッケージをインストール
pip install -r requirements.txt

# アプリケーションを起動
python run.py --MAX_SIZE="$MAX_SIZE" --TIMELINE_THUMBNAIL_NUM="$TIMELINE_THUMBNAIL_NUM"

# 仮想環境をデアクティベート
deactivate
