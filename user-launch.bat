@echo off

:: ユーザカスタム設定

:: 作業用プレビュー画面の最大サイズを指定(px)
set MAX_SIZE=640

:: タイムラインサムネイルの最大表示数を指定
set TIMELINE_THUMBNAIL_NUM=100


call launch.bat %MAX_SIZE% %TIMELINE_THUMBNAIL_NUM%


